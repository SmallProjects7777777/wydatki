package org.example;

import java.util.ArrayList;
import java.util.List;

public class ExpenseManager {
    List<Expense> expenses = new ArrayList<>();

    public void displayAllExpenses() {
        expenses.forEach(expense -> {
            System.out.println(expense.descriptions());
            System.out.println(expense.month());
            System.out.println(expense.type());
            System.out.println(expense.value());
        });
    }
}
