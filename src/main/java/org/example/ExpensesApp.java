package org.example;

import java.util.Scanner;

public class ExpensesApp {
    public void start() {
        Scanner scanner = new Scanner(System.in);

        while(true) {
            System.out.println("1. Wyświetl wszystkie wydatki");
            System.out.println("2. Wyświetl wydatki z wybranego miesiąca");
            System.out.println("3. Dodaj wydatek");
            System.out.println("4. Zakończ aplikacje");
            System.out.println("Wybierz opcje");

            int input = Integer.parseInt(scanner.nextLine());

            switch (input){
                case 1 -> System.out.println("wyświetl wydatki");
                case 2 -> System.out.println("wydatki z miesiąca");
                case 3 -> System.out.println("Dodaj wydatek");
                case 4 -> {
                    System.out.println("Zakończ aplikacje");
                    scanner.close();
                    System.exit(0);
                }
            }
        }
    }
}
